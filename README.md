# osm-routing-dev

OpenSM fork to add various new and/or experimental routing engines.

Available branches:
  - [SSSP and DFSSSP](https://gitlab.com/domke/osm-routing-dev/tree/dfsssp-3.3.20)
  - [Scheduling-aware routing (SAR)](https://gitlab.com/domke/osm-routing-dev/tree/sar-3.3.20)
  - [Nue routing](https://gitlab.com/domke/osm-routing-dev/tree/nue-3.3.20)
  - (master kept in sync with official OpenSM release)

